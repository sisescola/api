﻿using System;
using System.IO;
using System.Text.Json;
public class Program
{
    private static void Main(string[] args)
    {
        string json = File.ReadAllText("share/Mensagens.json");

        // Parse the JSON string into a JSON document
        JsonDocument jsonDocument = JsonDocument.Parse(json);

        // Access the JSON data
        foreach (JsonProperty property in jsonDocument.RootElement.EnumerateObject())
        {
            string propertyName = property.Name;
            JsonElement propertyValue = property.Value;

            Console.WriteLine($"{propertyName}: {propertyValue}");
        }
    }
}